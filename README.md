[![pipeline status](https://gitlab.com/vandud/dr-web-dev-ops-test/badges/master/pipeline.svg)](https://gitlab.com/vandud/dr-web-dev-ops-test/-/commits/master)
[![coverage report](https://gitlab.com/vandud/dr-web-dev-ops-test/badges/master/coverage.svg)](https://gitlab.com/vandud/dr-web-dev-ops-test/-/commits/master)  

Сборка проекта:
```
python setup.py bdist_wheel
```

Запуск тестов:
```
pip install -r requirements/development.txt
pytest tests --cov package
```
